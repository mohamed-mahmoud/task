@extends('admin.master')

{{--<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">--}}


@section('content')
    <br>
    <div class="mws-panel-header" style="width: 1030px">
        <button type="button" class="btn btn-secondary" style="font-size: 20px;"><a
                    href="{{url('section/insert')}}">Create Section</a></button>

    </div><br>
    <div class="mws-panel-body no-padding" style="width: 1050px">
        <table class="mws-datatable-fn mws-table" style="width: 1050px">
            <thead style="width: 1050px">
            <tr style="width: 1050px">

                <th>Number</th>
                <th>Title</th>
                <th>Description</th>
                <th>Body</th>

                <th>Operations</th>

            </tr>
            </thead>
            @if(isset($section))
                @foreach($section as $value)
                    <tr class="mws-datatable-fn mws-table" style="text-align: center">
                        <td class="cell100 column1">{!! $value->id !!}</td>
                        <td class="cell100 column1">{!! $value->title !!}</td>
                        <td class="cell100 column1">{!! $value->description !!}</td>
                        <td class="cell100 column1">{!! $value->body !!}</td>

                        {{--<td class="cell100 column1">{!! $value->sections !!}</td>--}}
                        <td>
                            <a href="{{url('section/show',$value->id)}}">
                                <i class="icol-eye" aria-hidden="true">
                                </i></a>
                            <a href="{{url('section/edit',$value->id)}}">
                                <li class="icol-application-edit">
                                </li>
                            </a>

                            <a href="{{url('section/delete',$value->id)}}"
                               onclick="return confirm('Are you sure you want to delete this item?');"><i
                                        class="icol-application-delete"
                                        aria-hidden="true">
                                </i></a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </table>
        {{ $section->links() }}
    </div><br>

@endsection