
@extends('admin.master')
@section('content')

    @if (session('messge'))
        <div class="btn-success" style="height:15 px;width: 950px;font-size: 18px;"   >
            {{ session('messge') }}
        </div>
    @endif
    <br>
    <div class="mws-panel grid_8">
        <div class="mws-panel-header">
            <span>Show  {{$section->title}}</span>
        </div>
        <div class="mws-panel-body no-padding">
            <form class="mws-form" action="{{url('/section/update',$section->id)}}" method="POST" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="mws-form-inline">

                    <div class="mws-form-row">
                        <label class="mws-form-label">  Title</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" required value="{{$section->title}}" placeholder="Write a Title" name="title">
                        </div>
                    </div>


                    <div class="mws-form-row">
                        <label class="mws-form-label">  Description</label>
                        <div class="mws-form-item">
                            <textarea  class="small" name="description" required placeholder="Write a description">{{$section->description}}</textarea>
                        </div>
                    </div>

                    <div class="mws-form-row">
                        <label class="mws-form-label">  Body</label>
                        <div class="mws-form-item">
                            <textarea  class="small" name="body" required placeholder="Write a Body">{{$section->body}}</textarea>
                        </div>
                    </div>


                    <div class="mws-form-row">
                        <label class="mws-form-label">Main Section</label>
                        <div class="mws-form-item">
                            <select  class="small" name="mainSection_id"  required>
                                <option></option>
                                @foreach($mainSections as $main )
                                    @if($main->id == $section->mainSection_id)
                                        <option  value="{{$main->id}}" selected> {{$main->title}} </option>
                                    @else
                                        <option  value="{{$main->id}}"> {{$main->title}} </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>

            </form>
        </div>
    </div>
@endsection

