
@extends('admin.master')
@section('content')

    @if (session('messge'))
        <div class="btn-success" style="height:15 px;width: 950px;font-size: 18px;"   >
            {{ session('messge') }}
        </div>
    @endif
    <br>
    <div class="mws-panel grid_8">
        <div class="mws-panel-header">
            <span>{{$mainCat->title}}</span>
        </div>
        <div class="mws-panel-body no-padding">
            <form class="mws-form" action="{{url('mainSection/show/')}}{{$mainCat->id}}" method="POST" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="mws-form-inline">

                    <div class="mws-form-row">
                        <label class="mws-form-label">  Title</label>
                        <div class="mws-form-item">
                            <input type="text" class="small" value="{{$mainCat->title}}" placeholder="Write a Title" name="title">
                        </div>
                    </div>


                    <div class="mws-form-row">
                        <label class="mws-form-label">  Description</label>
                        <div class="mws-form-item">
                            <textarea  class="small" name="description" placeholder="Write a description">{{$mainCat->description}}</textarea>
                        </div>
                    </div>



                </div>
            {{--    <div class="mws-button-row">
                    <input type="submit" value="Submit" class="btn btn-danger">
                    <input type="reset" value="Reset" class="btn ">
                </div>--}}
            </form>
        </div>
    </div>
@endsection

