@extends('admin.master')

{{--<link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}">--}}


@section('content')
    <br>
    <div class="mws-panel-header" style="width: 1030px">
        <button type="button" class="btn btn-secondary" style="font-size: 20px;"><a
                    href="{{url('section/insert')}}">Create Section</a></button>

    </div><br>
    <div class="mws-panel-body no-padding" style="width: 1050px">
        <table class="mws-datatable-fn mws-table" style="width: 1050px">
            <thead style="width: 1050px">
            <tr style="width: 1050px">

                <th>Number</th>
                <th>Title</th>
                <th>Description</th>
                <th>Body</th>


            </tr>
            </thead>
            @if(isset($sections))
                @foreach($sections as $value)
                    <tr class="mws-datatable-fn mws-table">
                        <td class="cell100 column1">{!! $value->id !!}</td>
                        <td class="cell100 column1">{!! $value->title !!}</td>
                        <td class="cell100 column1">{!! $value->body !!}</td>


                    </tr>
                @endforeach
            @endif
        </table>

    </div><br>

@endsection