<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'mainSection'], function() {
    Route::get('/', 'mainSectionsController@index');
    Route::get('getSections/{id}', 'mainSectionsController@get_sections');
    Route::get('edit/{id}', 'mainSectionsController@edit');
    Route::get('show/{id}', 'mainSectionsController@show');
    Route::get('insert', 'mainSectionsController@insert');
    Route::get('delete/{id}', 'mainSectionsController@delete');
    Route::post('store', 'mainSectionsController@store');
    Route::post('update/{id}', 'mainSectionsController@update');
});


Route::group(['prefix' => 'section'], function() {
    Route::get('/', 'SectionsController@index');
    Route::get('edit/{id}', 'SectionsController@edit');
    Route::get('show/{id}', 'SectionsController@show');
    Route::get('insert', 'SectionsController@insert');
    Route::get('delete/{id}', 'SectionsController@delete');
    Route::post('store', 'SectionsController@store');
    Route::post('update/{id}', 'SectionsController@update');
});