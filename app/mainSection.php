<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Section;
class mainSection extends Model
{
    protected $table="mainsections";
    protected $fillable=['title','description'];

    public function sections()
    {
        return $this->hasMany('App\Section','mainSection_id');
    }
}
