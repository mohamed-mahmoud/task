<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\mainSection;
use App\Section;

class SectionsController extends Controller
{
    public function index()
    {
        $section = Section::query()->paginate(2);

        return view('admin.sections.index', compact('section'));
    }

    public function show($id)
    {
        $mainSections = mainSection::all();
        $section = Section::find($id);
        return view('admin.sections.show', compact('section','mainSections'));
    }

    public function edit($id)
    {
        $mainSections = mainSection::all();
        $section = Section::find($id);
        return view('admin.sections.edit', compact('section','mainSections'));
    }

    public function update($id, Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'body' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $section = Section::find($id);
        $section->update($request->all());
//
//        $section->title = $request->title;
//        $section->description = $request->description;
//        $section->body = $request->body;
//

     //   $section->save();
        return redirect('/section');
    }

    public function insert()
    {
        $mainSections = mainSection::all();
        return view('admin.sections.insert', compact('mainSections'));
    }

    public function store(Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'title' => 'required',
            'body' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
//        $section = new mainSection();
//        $section->title = $request->title;
//        $section->body = $request->body;
//        $section->description = $request->description;
//        $section->save();


        Section::create($request->all());
        return redirect('/section');
    }

    public function delete($id)
    {
        $deletedField = DB::table('sections')->where('id', '=', $id)->delete();

        return redirect('/section');
    }
}
