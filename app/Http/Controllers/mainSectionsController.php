<?php

namespace App\Http\Controllers;

use App\Section;
use Dotenv\Validator;
use Illuminate\Http\Request;
use DB;
use App\mainSection;
class mainSectionsController extends Controller
{

    public function index()
    {
        $mainCat=mainSection::query()->paginate(2);

        return view('admin.main_section.index',compact('mainCat'));
    }

    public function get_sections($id){

      //  $sections= mainSection::with('sections')->where('id',$id)->first();
        $sections = Section::query()->where('mainSection_id','=',$id)->get();
        return view('admin.main_section.sections',compact('sections'));

    }

    public function show($id)
    {
        $mainCat=mainSection::find($id);
        return view('admin.main_section.show',compact('mainCat'));
    }

    public function edit($id)
    {
        $mainCat=mainSection::find($id);
        return view('admin.main_section.edit',compact('mainCat'));
    }

    public function update($id,Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
         $mainCat=mainSection::find($id);
         $mainCat->title=$request->title;
         $mainCat->description=$request->description;
         $mainCat->save();
        return redirect('/mainSection');
    }

    public function insert()
    {
        return view('admin.main_section.insert');
    }

    public function store(Request $request)
    {
        $validator = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $mainCat=new mainSection();
        $mainCat->title=$request->title;
        $mainCat->description=$request->description;
        $mainCat->save();
        return redirect('/mainSection');
    }

    public function delete($id)
    {
        $deletedField=DB::table('mainsections')->where('id','=',$id)->delete();

        return redirect('/mainSection');
    }
}
